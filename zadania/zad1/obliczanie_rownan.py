import numpy as np

wspolczynniki_lewa = [0.75, 0.9]
y = [260, 300]

# Przyklad dla naszej grupy
# (1 - [0.1, 0.25])x <= [260,300]


def oblicz_wartosc_importu(wspolczynniki_lewa, y):
    x_opt = y[0] / wspolczynniki_lewa[0]
    y_opt = [a * x_opt for a in wspolczynniki_lewa]
    print(f"y_opt: {y_opt}")

    W = wspolczynniki_lewa[0] - wspolczynniki_lewa[1]
    print(f"W: {W}")

    X = (y[0] - y[1]) / W
    print(f"X: {X}")

    # import
    I = ((wspolczynniki_lewa[0] * y[1]) - (y[0] * wspolczynniki_lewa[1])) / W
    # print(f"Watosc jednostek importu: {I}")

    # Sprawdzenie
    yL = [wspolczynniki_lewa[0] * X + I, wspolczynniki_lewa[1] * X + I]
    # print(f"Sprawdzenie: {yL}")

    return I


def oblicz_Hposs(wspolczynniki, y):

    XL = y[0] / wspolczynniki[0]
    XR = y[1] / wspolczynniki[1]

    XL_XR = [XL, XR]

    X_Poss = []
    for X in XL_XR:
        przedzial = [wspolczynniki[0] * X,
                     wspolczynniki[1] * X]
        dlugosc_przedzialu_oryginalny = y[1] - y[0]
        dlugosc_przedzialu_zalozony = przedzial[1] - przedzial[0]
        Hposs = dlugosc_przedzialu_oryginalny / dlugosc_przedzialu_zalozony
        X_Poss.append(Hposs)
    return (X_Poss, XL_XR)


def obliczanie_czesci_wspolnej_przedial(interval1, interval2):
    # Sortowanie przedziałów po wartości minimalnej
    sorted_intervals = sorted([interval1, interval2], key=lambda x: x[0])

    # Sprawdzenie czy istnieje część wspólna
    if sorted_intervals[0][1] < sorted_intervals[1][0]:
        return None  # Brak części wspólnej

    # Obliczenie części wspólnej
    xmin = sorted_intervals[1][0]
    xmax = min(sorted_intervals[0][1], sorted_intervals[1][1])

    return [xmin, xmax]


def oblicz_najlepszy_import(wspolczynniki, y, x):
    XL = wspolczynniki[0] * x
    XR = wspolczynniki[1] * x

    # hPoss powinno byc obliczone z czesci wspolnej, a nie z calego przedzialu
    dlugosc_przedzialu_zalozony = XR - XL
    czesc_wspolna_przedzial = obliczanie_czesci_wspolnej_przedial(y, [XL, XR])
    dlugosc_czesci_wspolnej = czesc_wspolna_przedzial[1] - \
        czesc_wspolna_przedzial[0]
    Hposs = dlugosc_czesci_wspolnej / dlugosc_przedzialu_zalozony

    return Hposs


def oblicz_linspace(start, end, step):
    num_elements = int(np.round((end - start) / step)) + 1
    return np.linspace(start, end, num_elements)


def oblicz_hBest_xBest_yBest(linspaceX, wspolczynniki_lewa, y):
    h_best = 0
    x_best = 0
    for x in linspaceX:
        h = oblicz_najlepszy_import(wspolczynniki_lewa, y, x)
        if (h > h_best):
            h_best = h
            x_best = x

    return (h_best, x_best)
