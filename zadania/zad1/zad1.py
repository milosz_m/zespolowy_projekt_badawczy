from zadania.zad1.obliczanie_rownan import oblicz_Hposs, oblicz_hBest_xBest_yBest, oblicz_linspace, oblicz_wartosc_importu, wspolczynniki_lewa, y


def oblicz_zad1():
    import_produktu = oblicz_wartosc_importu(wspolczynniki_lewa, y)
    print(f"Watosc jednostek importu: {import_produktu}")

    # [0.75, 0.9]x <= [260, 300]
    Hposs = oblicz_Hposs(wspolczynniki_lewa, y)
    XL_XR = Hposs[1]
    Hposs = Hposs[0]

    print(f"Hposs: ", Hposs)
    print(f"Granice XL_XR: ", XL_XR)

    linspaceX = oblicz_linspace((260/0.9), (300/0.75), 0.1)

    h_best, x_best = oblicz_hBest_xBest_yBest(
        linspaceX, wspolczynniki_lewa, y)

    print(f"Najwyzsza wartosc prawdopodobienstwa zmieszczenia sie w zalozeniach produkcji (h_best): ", h_best)
    print(f"Optynalna wartosc produkcji dla zalozonych potrzeb spolecznych (x_best): ", x_best)
